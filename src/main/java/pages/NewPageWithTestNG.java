package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class NewPageWithTestNG {
@Test	
public void RunMethod() throws InterruptedException {
	

	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

	// launch browser
	ChromeDriver driver = new ChromeDriver();
	
	// to maximise the Browser
    driver.manage().window().maximize();

	// load url
	driver.get("https://acme-test.uipath.com/account/login");
	
	// enter email
	driver.findElementById("email").sendKeys("saiprashanth1407@yahoo.com");

	// enter password
	driver.findElementById("password").sendKeys("Newpass@123");
	
	// click login
	driver.findElementById("buttonLogin").click();
	
	//Thread.sleep
	Thread.sleep(3000);
	
	//Click on Vendor
	Actions action=new Actions(driver);
	WebElement vendor = driver.findElementByXPath("//div[5]//button[1]");
	action.moveToElement(vendor).perform();
	
	//Click on Search Vendor
	driver.findElementByXPath("//a[contains(text(),'Search for Vendor')]").click();
	
	//Enter Vendor Tax
   driver.findElementById("vendorTaxID").sendKeys("FR329083");
   
   //Click on Search
   driver.findElementById("buttonSearch").click();
	
   // //table//tr//td[1]
   String vendortext = driver.findElementByXPath("//table//tr//td[1]").getText();
   System.out.println("vendor name is: " +vendortext);

}
}
